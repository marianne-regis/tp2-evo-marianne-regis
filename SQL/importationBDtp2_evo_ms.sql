-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 20 oct. 2017 à 00:47
-- Version du serveur :  5.7.19
-- Version de PHP :  5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `tp2_evo_ms`
--

-- --------------------------------------------------------

--
-- Structure de la table `clients`
--

DROP TABLE IF EXISTS `clients`;
CREATE TABLE IF NOT EXISTS `clients` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) COLLATE latin1_bin NOT NULL,
  `prenom` varchar(50) COLLATE latin1_bin NOT NULL,
  `adresse` varchar(50) COLLATE latin1_bin NOT NULL,
  `ville` varchar(50) COLLATE latin1_bin NOT NULL,
  `province` varchar(40) COLLATE latin1_bin NOT NULL,
  `codePostal` char(7) COLLATE latin1_bin NOT NULL,
  `login` varchar(20) COLLATE latin1_bin NOT NULL,
  `motPasse` varchar(80) COLLATE latin1_bin NOT NULL,
  `email` varchar(50) COLLATE latin1_bin NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

--
-- Déchargement des données de la table `clients`
--

INSERT INTO `clients` (`no`, `nom`, `prenom`, `adresse`, `ville`, `province`, `codePostal`, `login`, `motPasse`, `email`) VALUES
(1, 'Tremblay', 'Eric', '1444 rue Duclos', 'Québec', 'Qc', 'G1S3E6', 'Eric', 'toto01', 'tremblayeric@gmail.com'),
(2, 'Brousseau', 'Jacques', '2058 ave Dupré', 'Québec', 'Qc', 'G3E1E3', 'JackBr', 'brou12', 'brousseaujacques@gmail.com'),
(3, 'Simard', 'Mathieu', '345, 2e rue', 'Québec', 'Quebec', 'G3G 3G3', 'simpsonsimard', '12345', 'simpsonsimard@hotmail.com');

-- --------------------------------------------------------

--
-- Structure de la table `commandes`
--

DROP TABLE IF EXISTS `commandes`;
CREATE TABLE IF NOT EXISTS `commandes` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `statut` varchar(20) COLLATE latin1_bin NOT NULL,
  `typePaiement` varchar(20) COLLATE latin1_bin NOT NULL,
  `noCarte` text COLLATE latin1_bin,
  `noClient` int(11) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

--
-- Déchargement des données de la table `commandes`
--

INSERT INTO `commandes` (`no`, `date`, `statut`, `typePaiement`, `noCarte`, `noClient`) VALUES
(10, '2017-10-19', '1', 'master', '1111222244445555', 3),
(11, '2017-10-19', '1', 'master', '1234567890123456', 3);

-- --------------------------------------------------------

--
-- Structure de la table `items_commande`
--

DROP TABLE IF EXISTS `items_commande`;
CREATE TABLE IF NOT EXISTS `items_commande` (
  `noCommande` int(11) NOT NULL,
  `noProduit` int(11) NOT NULL,
  `qte` int(11) NOT NULL,
  PRIMARY KEY (`noCommande`,`noProduit`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

--
-- Déchargement des données de la table `items_commande`
--

INSERT INTO `items_commande` (`noCommande`, `noProduit`, `qte`) VALUES
(10, 3, 1),
(10, 5, 1),
(11, 4, 1);

-- --------------------------------------------------------

--
-- Structure de la table `produits`
--

DROP TABLE IF EXISTS `produits`;
CREATE TABLE IF NOT EXISTS `produits` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) COLLATE latin1_bin NOT NULL,
  `description` varchar(500) COLLATE latin1_bin NOT NULL,
  `prix` decimal(7,2) NOT NULL,
  `qte` int(11) NOT NULL,
  `dateParution` date NOT NULL,
  `image` varchar(50) COLLATE latin1_bin NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

--
-- Déchargement des données de la table `produits`
--

INSERT INTO `produits` (`no`, `nom`, `description`, `prix`, `qte`, `dateParution`, `image`) VALUES
(1, 'Canne à pêche Tiagra', 'La nouvelle génération des cannes de traîne Tiagra sont les XTR-B : son carbone XT250 + Biofibre, déjà très résistant, s\'améliore encore grâce à la technologie HPC (carbone haute pression), qui rend le blank plus léger et plus fin.', '150.00', 15, '2017-06-02', 'assets/img/produit/canne.jpg'),
(2, 'Lunettes Oakley', 'A la fois sportswear et tendances, les lunettes de soleil OAKLEY OO 9096 plaisent à tous par leur design et leur légèreté.', '150.00', 42, '2017-06-07', 'assets/img/produit/lunettes.jpg'),
(3, 'Ensemble de golf Wilson', 'Ensemble de golf Wilson droitier pour hommes, contenant 12 bâtons. Sac inclus.', '525.00', 10, '2017-06-08', 'assets/img/produit/golf.jpg'),
(4, 'Raquette de tennis Wilson', 'Raquette de tennis Wilson Intrigue, pour femme.', '25.75', 12, '2017-06-08', 'assets/img/produit/tennis.jpg'),
(5, 'Bâton de hockey SherWood', 'Bâton de hockey senior Ek45 Grip. \r\n', '65.00', 32, '2017-06-08', 'assets/img/produit/hockey.jpg');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
