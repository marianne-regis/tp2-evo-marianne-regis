<?php
/**
 * Created by PhpStorm.
 * User: Marianne
 * Date: 2017-11-02
 * Time: 13:46
 */
$pageTitle = "Commentaires";
require_once('include/header.php'); ?>
<div class="container">
<?php if (isset($_GET['code']))
{
    ?>
    <div class="alert error">
        <p>
            <?php
            if ($_GET['code']==1) {
                echo 'Vous devez être connecté pour pouvoir écrire un commentaire.';
            }
            if ($_GET['code']==2) {
                echo 'Veuillez saisir un commentaire.';
            }
            ?>
        </p>
    </div>
    <?php
}?>

    <h2>Commentaires</h2>
    <form class="formulaireConnection row" action="control/comment/ajouter.php" method="POST">
        <title>Les commentaires</title>
        <label for="comment">Votre commentaire :</label>
        <textarea id="comment" name="texte"></textarea>
        <input type="submit" id="envoyer" value="Envoyer">
    </form>

<?php
require('control/param_bd.inc');
$req = $connection->prepare('SELECT * FROM commentaire');
$req->execute();
while($comment = $req->fetch()){
?>
<div class="card">
    <p><em><?php echo $comment['dateCommentaire'].' - '. $comment['username'].' ' ?> a écrit: </em> <?php echo ' '.$comment['texte']?></p>
</div>
<?php
}
$req->closeCursor();
$connection = null; ?>
</div>
<?php require_once('include/footer.php'); ?>
</body>
</html>