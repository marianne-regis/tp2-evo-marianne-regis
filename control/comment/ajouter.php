<?php
/**
 * Created by PhpStorm.
 * User: Marianne
 * Date: 2017-11-02
 * Time: 14:13
 */
session_start();
$current_url = explode('?',  $_SERVER['HTTP_REFERER']);
    require('../param_bd.inc');
    if(isset($_SESSION['utilisateur']))
    {
        if(!empty($_POST['texte'])){
            $req = $connection->prepare('INSERT INTO commentaire(texte, username) VALUES (:texte,:username)');
            $req->execute(array(':texte'=>htmlspecialchars($_POST['texte']),
                ':username'=>$_SESSION['utilisateur']['prenom']));
            header('Location: ' . $current_url[0]);
            exit();
        }
        else{
            header('Location: ' . $current_url[0]."?code=2");
            exit();
        }

    }else {
        header('Location: ' . $current_url[0]."?code=1");
        exit();
    }

?>