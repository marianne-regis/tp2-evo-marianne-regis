<?php
/*
* Titre : ajout.php
* Description : Permet l'ajout de produits dans le tableau panier
*/
session_start();
require('../param_bd.inc');
$current_url = explode('?',  $_SERVER['HTTP_REFERER']);
$req = $connection->prepare('SELECT qte FROM produits WHERE no = :arraydonnees');
$req->execute(array('arraydonnees'=> $_POST['produit']));
$produit = $req->fetch();
var_dump($current_url );


//produit non disponible en quantité suffisante
if ($produit['qte']<$_POST['qte']) {
    header('Location: ' . $current_url[0].'?'.$current_url[1]."&code=1");
    exit();
}


if (isset($_POST['produit'])) {
  if (isset($_SESSION['panier'])) {
    $checkexistence = array_search($_POST['produit'],$_SESSION['panier']['no']);
    if ($checkexistence === false) {
      array_push($_SESSION['panier']['no'], $_POST['produit']);
      array_push($_SESSION['panier']['qte'], $_POST['qte']);
    }else {
      $_SESSION['panier']['qte'][$checkexistence]+=$_POST['qte'];
    }
  //Si le panier n'existe pas on le crée avec les premières valeurs envoyées par l'utilisateur.
  }else {
    $_SESSION['panier']=array();
    $_SESSION['panier']['no']=array($_POST['produit']);
    $_SESSION['panier']['qte']=array($_POST['qte']);
  }
}
//on ramène l'utilisateur sur la page précédente
  header('Location: ' . $current_url[0].'?'.$current_url[1]."&code=0");
  exit();

?>
