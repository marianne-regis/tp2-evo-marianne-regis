<?php
/*
* Titre : remove.php
* Description : Retire une information du tableau de panier
* voir : /control/panier/afficherpanier pour post (bouton supprimer)
*/
session_start();
//on regarde si les info viennent d'un poste du panier
if (isset($_POST['delete'])) {
  //on supprime le numero produit du array
  unset($_SESSION['panier']['no'][$_POST['delete']]);
  //on supprime la quantité du produit du array
  unset($_SESSION['panier']['qte'][$_POST['delete']]);
  //On reconstruit les index du array pour avoir 0,1,2,3 et non 0,1,3 qui donnerait une erreur dans l'affichage des produits
  $_SESSION['panier']['no']=array_values($_SESSION['panier']['no']);
  //On reconstruit les index du array pour avoir 0,1,2,3 et non 0,1,3 qui donnerait une erreur dans l'affichage des produits
  $_SESSION['panier']['qte']=array_values($_SESSION['panier']['qte']);
}
//on renvoie l'utilisateur sur la derniere page consultée
header('Location: ' . $_SERVER['HTTP_REFERER']);

?>
