<?php
/*
* Titre : login.php
* Description : Vérifie si l'utilisateur existe et que son mot de passe est correct
* voir : voir /login.php pour post
*/
session_start();
$loginpage="../../login.php";
if(isset($_POST['user'])){
  if(empty($_POST['user']) || empty($_POST['password'])){
    header('location: '.$loginpage.'?code=1');
    /*champs vides*/
    exit;
  }else{
    $utilisateur = $_POST['user'];
    $password = $_POST['password'];
    require('../param_bd.inc');
    $req = $connection->prepare('SELECT * FROM clients WHERE login= :user AND motPasse= :password');
    $req->execute(array('user'=> $utilisateur, 'password'=> $password));
    $resultat = $req->fetch();
    $req->closeCursor();
    $conn = null;
    if ($req->rowCount()!=0) {
      $_SESSION['utilisateur']=array('no'=>$resultat['no'],'nom'=>$resultat['nom'],'prenom'=>$resultat['prenom']);
      header('location: ../../index.php');
    }else {
      header('location: '.$loginpage.'?code=2');
      /*champs sont incorrects*/
    }
  }
}
