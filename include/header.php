<?php
/*
* Titre : header.php
* Description : Fichier de configuration du header du site internet
*/
session_start(); include_once('include/head.php'); ?>
<body>
  <header>
    <div class="container">
      <div class="header1">
        <ul class="socialcontainer">
          <li class="left"><a href="panier.php"><i class="fa fa-shopping-bag"><?php
          if (isset($_SESSION["panier"])) {
            echo array_sum($_SESSION["panier"]['qte']);
          }else {
            echo '0';
          }?></i></a></li>
          <?php
          if(isset($_SESSION['utilisateur'])){
            echo "<li><a href='profil.php'>".$_SESSION['utilisateur']['prenom']."</a></li>";
            echo "<li><a href='control/utilisateur/logout.php'>Déconnexion</a></li>";
          }else{
            ?>
            <li><a href="login.php">Connexion</a></li>
            <li><a href="register.php">Inscription</a></li>
            <?php
          }
          ?>
        </ul>
      </div><!--/header1 clearfix -->
      <div class="header2">
        <div class="logo">
          <a href="index.php"><h1 hidden="hidden">Boutique le sportif</h1><img src="assets/img/index/Sportif.png" alt="Boutique le sportif"/></a>
        </div>
        <nav>
          <ul>
            <li><a href="index.php">Accueil</a></li>
            <li><a href="produits.php">Produits</a></li>
              <li><a href="profil.php">Profil</a></li>
              <li><a href="panier.php">Panier</a></li>
              <li><a href="comments.php">Commentaires</a></li>
              <li><a href="accessibilite.php">Accessibilité</a></li>
              <li><a href="plansite.php">Plan du site</a></li>
          </ul>
        </nav>
      </div><!--/header2 -->
    </div>
  </header>