<?php
/**
 * Created by PhpStorm.
 * User: Marianne
 * Date: 2017-11-03
 * Time: 16:30
 */
$pageTitle = "Plan du site";
include_once('include/header.php');
?>
<div class="container">
    <h2>Plan du site</h2>
    <ul>
        <li><a href="index.php">Accueil</a></li>
        <li><a href="produits.php">Produits</a></li>
        <li><a href="profil.php">Profil</a></li>
        <li><a href="panier.php">Panier</a></li>
        <li><a href="comments.php">Commentaires</a></li>
        <li><a href="accessibilite.php">Accessibilité</a></li>
    </ul>
</div>
<?php require_once('include/footer.php'); ?>
</body>
</html>
