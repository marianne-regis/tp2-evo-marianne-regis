
    <?php
    /*
    * Titre : profil.php
    * Description : affiche les informations du client
    */
    $pageTitle = "Profil";
    include_once('include/header.php');

    if (!isset($_SESSION['utilisateur']['no'])) {
      header('Location: login.php');
      exit;
    }else{
      require('control/param_bd.inc');
      $req = $connection->prepare('SELECT * FROM clients WHERE no= :no');
      $req->execute(array('no'=> $_SESSION['utilisateur']['no']));
      $resulta = $req->fetch();
      $req->closeCursor();
      $conn = null;
    ?>

    <div class="container">
        <h2>Votre profil</h2>
        <form class="formulaireConnection" action="control/utilisateur/edit.php" method="post">
          <label for="name">Nom :</label>
          <input type="text" id="" value="<?php echo $resulta['nom'] ?>" name="nom"><br>
          <label for="prenom">Prénom :</label>
          <input type="text" id="" value="<?php echo $resulta['prenom'] ?>" name="prenom"><br>
          <label for="user">Nom d'utilisateur :</label>
          <input type="text" id="" value="<?php echo $resulta['login'] ?>" name="login" disabled><br>
          <label for="email">Courriel :</label>
          <input type="email" id="" value="<?php echo $resulta['email'] ?>" name="email" email><br>
          <label for="prenom">Adresse :</label>
          <input type="text" id="" value="<?php echo $resulta['adresse'] ?>" name="adresse"><br>
          <label for="ville">Ville :</label>
          <input type="text" id="" value="<?php echo $resulta['ville'] ?>" name="ville"><br>
          <label for="province">Province :</label>
          <input type="text" id="" value="<?php echo $resulta['province'] ?>" name="province"><br>
          <label for="codePostal">Code Postal :</label>
          <input type="text" id="" value="<?php echo $resulta['codePostal'] ?>" name="codePostal"><br>
          <input type="submit" id="" value="Enregistrer">
        </form>
    </div>
    <?php } require_once('include/footer.php'); ?>
</body>
</html>
